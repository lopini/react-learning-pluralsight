import React from "react";

const Button = (props) => {
    const handleClick = () => props.incrementCounter(props.increment);

    return (
        <button
            onClick={handleClick}>
            +{props.increment}
        </button>
    )
}

export default Button;