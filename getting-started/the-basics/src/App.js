import './App.css';
import React from "react";
import Button from "./components/button/Button";
import Display from "./components/display/Display";

function App() {
  const [counter, changeCounter] = React.useState(1);
  const incrementCounter = (increment) => {
    changeCounter(counter + increment);
  }

  return (
    <div>
        Counter
        <Button incrementCounter={incrementCounter} increment={1} />
        <Button incrementCounter={incrementCounter} increment={2} />
        <Button incrementCounter={incrementCounter} increment={5} />
        <Button incrementCounter={incrementCounter} increment={10} />
        <Display counter={counter} />
    </div>
  );
}

export default App;
